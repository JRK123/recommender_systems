import pandas as pd
import numpy as np
import itertools
# from torch import cosine_similarity
from sklearn.metrics.pairwise import cosine_similarity
from neumf import NEUMF
import torch
from sklearn.manifold import TSNE
import seaborn as sns
import matplotlib.pyplot as plt


def massgingOfData(ml1m_rating):
    user_id = ml1m_rating[['uid']].drop_duplicates().reindex()
    user_id['userId'] = np.arange(len(user_id))
    ml1m_rating = pd.merge(ml1m_rating, user_id, on=['uid'], how='left')
    item_id = ml1m_rating[['mid']].drop_duplicates()
    item_id['itemId'] = np.arange(len(item_id))
    ml1m_rating = pd.merge(ml1m_rating, item_id, on=['mid'], how='left')
    ml1m_rating = ml1m_rating[['userId', 'itemId', 'rating', 'timestamp']]
    return ml1m_rating

def ReadFile(filename, seperator):
    # ml1m_dir = filename
    # ml1m_rating = pd.read_csv(ml1m_dir, delim_whitespace=True, header=None, names=['uid', 'mid', 'rating', 'timestamp'],  engine='python')
    ml1m_dir = filename
    ml1m_rating = pd.read_csv(ml1m_dir, sep=seperator, header=None, names=['uid', 'mid', 'rating', 'timestamp'],  engine='python')

    return ml1m_rating

def ExtractCoreUsers(dataframe):
    print("# of rows in ml1m_ratings: ", len(dataframe))
    u_len = len(dataframe['userId'].drop_duplicates())
    print("USER LEN:", u_len)
    # print(user_id)

    m_len = len(dataframe['itemId'].drop_duplicates())
    print("MOVIE LEN:", m_len)
    userItemMatrix = np.zeros(shape=(u_len, m_len))
    # print(userItemMatrix)

    for index, row in dataframe.iterrows():
        userItemMatrix[row['userId']][row['itemId']] = 1
        # print(row['uid'], row['mid'])
    print("USER ITEM MATRIX: \n", userItemMatrix)
    print("USER ITEM MATRIX SHAPE:", userItemMatrix.shape)

    df = pd.DataFrame(userItemMatrix)
    cosineSimilarity = cosine_similarity(df)
    print("SHAPE OF COSINE MATIX:\n ", cosineSimilarity.shape)

    listToStoreTopFiftyOfEveryUser = []
    for i in range(0, cosineSimilarity.shape[0]):
        idx = np.argpartition(cosineSimilarity[i], -50)[-50:]
        listToStoreTopFiftyOfEveryUser.append(idx)
    # print("Top fifty list: \n", listToStoreTopFiftyOfEveryUser)
    # listToStoreTopFiftyOfEveryUser = np.array(listToStoreTopFiftyOfEveryUser)
    flatten = np.concatenate(listToStoreTopFiftyOfEveryUser)
    listToStoreTopFiftyOfEveryUser = flatten.ravel()

    print("List of top 50", listToStoreTopFiftyOfEveryUser)
    df = pd.DataFrame(listToStoreTopFiftyOfEveryUser)
    allUserList = df.value_counts().index.tolist()
    print("ALL USERS LIST", allUserList)
    allUserList = list(sum(allUserList,()))
    print("ALL USERS LIST", allUserList)
    twentyPercentUserList = allUserList[:int(len(allUserList)*0.2)]
    # print("TWENTY PERCENT USER:", len(twentyPercentUserList))
    print("TWENTY PERCENT USER:", (twentyPercentUserList))
    coreusers = dataframe.iloc[np.where(dataframe.userId.isin(twentyPercentUserList))]
    print("CORE USERS:\n", coreusers)
    return coreusers
def ExtractEmbeddings(nuemf_pretrain_file_path):
    neumf_config = {'alias': 'pretrain_neumf_factor8neg4',
            'num_epoch': 15,
            'batch_size': 1024,
            'optimizer': 'adam',
            'adam_lr': 1e-3,
            'num_users': 55187,
            'num_items': 9916,
            'latent_dim_mf': 64,
            'latent_dim_mlp': 256,
            'num_negative': 4,
            'layers': [512,256,128,64],  # layers[0] is the concat of latent user vector & latent item vector
            'l2_regularization': 0,
            'use_cuda': True,
            'device_id': 0,
            'pretrain': True,
            'pretrain_mf': 'checkpoints/{}'.format('gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'),
            'pretrain_mlp': 'checkpoints/{}'.format('mlp_factor8neg4_bz256_166432168_pretrain_reg_0.0000001_Epoch14_HR0.5561_NDCG0.3142.model'),
            'model_dir':'checkpoints/{}_Epoch{}_HR{:.4f}_NDCG{:.4f}.model'
            }
    neumf_model = NEUMF(neumf_config)
    embedding_matrix_gmf = neumf_model.GetEmbeddings(neumf_model, nuemf_pretrain_file_path)
    print("GMF EMBEDDING MATRIX:\n", embedding_matrix_gmf)
    return embedding_matrix_gmf


def plotTsne(dataset, coreUsersList, coreuserdataset):
    print('core user len:', len(coreUsersList))
    tsne = TSNE(n_components=2, verbose=1, random_state=123)
    z = tsne.fit_transform(dataset)
    df2 = pd.DataFrame()
    
    df2["feature-1"] = z[:,0]
    df2["feature-2"] = z[:,1]
    dfclass = ['non-core-users'] * dataset.shape[0]
    for i in coreUsersList:
        dfclass[i] = 'core-users'
    df2["users-category"] = dfclass
    print(df2.groupby(['users-category']).size())
    sns.scatterplot(x="feature-2", y="feature-1", hue='users-category',
        data=df2).set(title="t-SNE Pininterest")   
    # plt.legend(loc = 2, bbox_to_anchor = (1,1)) 
    plt.show()
def main():
    # df = ReadFile('data/pin-interest-main.txt', '   ')
    # df = massgingOfData(df)
    # print("DATAFRAME", df)
    # cu = ExtractCoreUsers(df)
    # print(len(cu['userId'].drop_duplicates()))
    # # print(len(cu['itemId'].drop_duplicates()))
    # # cu.to_csv('data/pin_coreusers.csv', index=False)

    nuemf_pretrain_file_path = 'nuemf_state_files/nuemf_pin_64_256_0_sgd_Epoch0_HR0.8788_NDCG0.5538.model'
    gmf_embeddings = ExtractEmbeddings(nuemf_pretrain_file_path)
    print(gmf_embeddings.shape)
    cu = ReadFile('data/pin_coreusers.csv', ',')
    coreUsersList = cu['uid'].drop_duplicates()
    userEmbeddingList = []
    for user in coreUsersList:
        # print(user)
        userEmbeddingList.append(gmf_embeddings[user])
    print(len(userEmbeddingList))

    print('unique core users:', len(coreUsersList.unique()))
    plotTsne(gmf_embeddings, coreUsersList, userEmbeddingList)
    pass



if __name__ == "__main__":
    main()