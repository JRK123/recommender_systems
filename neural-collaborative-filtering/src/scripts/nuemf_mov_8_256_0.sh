#!/bin/sh
#SBATCH --job-name=test_job # Job name
#SBATCH --ntasks=1 # Run on a single CPU
#SBATCH --time=10:00:00 # Time limit hrs:min:sec
#SBATCH --output=./output/gmf%j.out # Standard output and error log
#SBATCH --gres=gpu:1
#SBATCH --partition=low_2h_1gpu
#This cell generates the results for GMF factor 8
# echo "GMF FACTOR 8"
# python train.py --model 'GMF' --alias 'gmf_factor8_neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "GMF FACTOR 16"
# python train.py --model 'GMF' --alias 'gmf_factor16neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model' 
# echo "GMF FACTOR 32"
# python train.py --model 'GMF' --alias 'gmf_factor32neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "GMF FACTOR 64"
# python train.py --model 'GMF' --alias 'gmf_factor64neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'


# echo "MLP FACTOR 8"
# python train.py --model 'MLP' --alias 'mlp_factor8neg4' --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "MLP FACTOR 16"
# python train.py --model 'MLP' --alias 'mlp_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model' 
# echo "MLP FACTOR 32"
# python train.py --model 'MLP' --alias 'mlp_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "MLP FACTOR 64"
# python train.py --model 'MLP' --alias 'mlp_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# # nuemf without pretraning : 
# echo "NEUMF NO PRETRAIN FACTOR 8"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor8neg4' --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model'
# echo "NEUMF NO PRETRAIN FACTOR 16"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --latent_dim_mf 16 --latent_dim_mlp 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor16neg4-implict_Epoch14_HR0.5197_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 32"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --latent_dim_mf 32 --latent_dim_mlp 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor32neg4-implict_Epoch14_HR0.5227_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 64"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --latent_dim_mf 64 --latent_dim_mlp 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor64neg4-implict_Epoch14_HR0.5252_NDCG0.2943.model'


# nuemf with pretrain: 
echo "NEUMF MOVIE FACTOR 8"
python train.py --model 'NEUMF' --alias 'nuemf_mov_8_256_0'  --layers '64,32,16,8' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_mov_8_256_0_Epoch81_HR0.6449_NDCG0.3690.model' --pretrain_mlp 'checkpoints/MLP/mlp_mov_8_256_0_Epoch62_HR0.6416_NDCG0.3727.model'
# echo "NEUMF MOVIE FACTOR 16"
# python train.py --model 'NEUMF' --alias 'nuemf_mov_16_256_0' --layers '128,64,32,16' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 16 --latent_dim_mf 16 --latent_dim_mlp 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_mov_16_256_0_Epoch92_HR0.6942_NDCG0.4123.model' --pretrain_mlp 'checkpoints/MLP/mlp_mov_16_256_0_Epoch33_HR0.6579_NDCG0.3916.model'
# echo "NEUMF MOVIE FACTOR 32"
# python train.py --model 'NEUMF' --alias 'nuemf_mov_32_256_0' --layers '256,128,64,32' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 32 --latent_dim_mf 32 --latent_dim_mlp 128 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_mov_32_256_0_Epoch96_HR0.7111_NDCG0.4298.model' --pretrain_mlp 'checkpoints/MLP/mlp_mov_32_256_0_Epoch18_HR0.6656_NDCG0.3918.model'
# echo "NEUMF MOVIE FACTOR 64"
# python train.py --model 'NEUMF' --alias 'nuemf_mov_64_256_0' --layers '512,256,128,64' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 64 --latent_dim_mf 64 --latent_dim_mlp 256 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_mov_64_256_0_Epoch53_HR0.6816_NDCG0.4124.model' --pretrain_mlp 'checkpoints/MLP/mlp_mov_64_256_0_Epoch12_HR0.6641_NDCG0.3924.model'


#------PININTEREST GMF-------------
# echo "PININTEREST GMF FACTOR 8"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor8neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST GMF FACTOR 16"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor16neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST GMF factor 32"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor32neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST GMF factor 64"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor64neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# #--------PININTEREST MLP--------------
# echo "PININTEREST MLP FACTOR 8"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor8neg4'  --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST MLP FACTOR 16"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST MLP factor 32"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST MLP factor 64"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'


# --------PININTEREST NEUMF WITHOUT PRETRANING--------------
# echo "NEUMF NO PRETRAIN FACTOR 8"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor8neg4' --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model'
# echo "NEUMF NO PRETRAIN FACTOR 16"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --latent_dim_mf 16 --latent_dim_mlp 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor16neg4-implict_Epoch14_HR0.5197_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 32"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --latent_dim_mf 32 --latent_dim_mlp 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor32neg4-implict_Epoch14_HR0.5227_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 64"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --latent_dim_mf 64 --latent_dim_mlp 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor64neg4-implict_Epoch14_HR0.5252_NDCG0.2943.model'
