#This cell generates the results for GMF factor 8
# echo "GMF FACTOR 8"
# python train.py --model 'GMF' --alias 'gmf_mov_8_256_0' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "GMF FACTOR 16"
# python train.py --model 'GMF' --alias 'gmf_mov_8_256_0' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 16 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model' 
echo "GMF FACTOR 32 BATCH 256"
python train.py --model 'GMF' --alias 'gmf_mov_32_256_0' --num_epoch 100 --batch_size 256 --optimizer 'adam' --adam_lr 0.0005 --latent_dim 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "GMF FACTOR 64"
# python train.py --model 'GMF' --alias 'gmf_mov_8_256_0' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'


# echo "MLP FACTOR 8"
# python train.py --model 'MLP' --alias 'mlp_factor8neg4' --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "MLP FACTOR 16"
# python train.py --model 'MLP' --alias 'mlp_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model' 
# echo "MLP FACTOR 32"
# python train.py --model 'MLP' --alias 'mlp_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "MLP FACTOR 64"
# python train.py --model 'MLP' --alias 'mlp_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# # nuemf without pretraning : 
# echo "NEUMF NO PRETRAIN FACTOR 8"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor8neg4' --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model'
# echo "NEUMF NO PRETRAIN FACTOR 16"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --latent_dim_mf 16 --latent_dim_mlp 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor16neg4-implict_Epoch14_HR0.5197_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 32"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --latent_dim_mf 32 --latent_dim_mlp 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor32neg4-implict_Epoch14_HR0.5227_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 64"
# python train.py --model 'NEUMF' --alias 'NEUMF_no_pretrain_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --latent_dim_mf 64 --latent_dim_mlp 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor64neg4-implict_Epoch14_HR0.5252_NDCG0.2943.model'


# nuemf with pretrain: 
# echo "NEUMF FACTOR 8"
# python train.py --model 'NEUMF' --alias 'NEUMF_pretrain_factor8neg4-  --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_factor8_neg4_Epoch27_HR0.6224_NDCG0.3539.model' --pretrain_mlp 'checkpoints/MLP/mlp_factor8neg4_Epoch58_HR0.6235_NDCG0.3570.model'
# echo "NEUMF FACTOR 16"
# python train.py --model 'NEUMF' --alias 'NEUMF_pretrain_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --latent_dim_mf 16 --latent_dim_mlp 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_factor16neg4_Epoch57_HR0.6608_NDCG0.3996.model' --pretrain_mlp 'checkpoints/MLP/mlp_factor16neg4_Epoch26_HR0.6440_NDCG0.3717.model'
# echo "NEUMF FACTOR 32"
# python train.py --model 'NEUMF' --alias 'NEUMF_pretrain_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --latent_dim_mf 32 --latent_dim_mlp 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_factor32neg4_Epoch21_HR0.6445_NDCG0.3781.model' --pretrain_mlp 'checkpoints/MLP/mlp_factor32neg4_Epoch18_HR0.6576_NDCG0.3844.model'
# echo "NEUMF FACTOR 64"
# python train.py --model 'NEUMF' --alias 'NEUMF_pretrain_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --latent_dim_mf 64 --latent_dim_mlp 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_factor64neg4_Epoch27_HR0.6528_NDCG0.3836.model' --pretrain_mlp 'checkpoints/MLP/mlp_factor64neg4_Epoch11_HR0.6614_NDCG0.3859.model'


# ------PININTEREST GMF-------------
# echo "PININTEREST GMF FACTOR 8 BATCH 256"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pin_8_256_0' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST GMF FACTOR 16"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor16neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST GMF factor 32"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor32neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST GMF factor 64"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'GMF' --alias 'gmf_pininterest_factor64neg4' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# #--------PININTEREST MLP--------------
# echo "PININTEREST MLP FACTOR 8"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor8neg4'  --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST MLP FACTOR 16"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST MLP factor 32"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
# echo "PININTEREST MLP factor 64"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'MLP' --alias 'mlp_pininterest_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'


# --------PININTEREST NEUMF WITHOUT PRETRANING--------------
# echo "NEUMF NO PRETRAIN FACTOR 8"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor8neg4' --layers '64,32,16,8' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model'
# echo "NEUMF NO PRETRAIN FACTOR 16"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor16neg4' --layers '128,64,32,16' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --latent_dim_mf 16 --latent_dim_mlp 64 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor16neg4-implict_Epoch14_HR0.5197_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 32"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor32neg4' --layers '256,128,64,32' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --latent_dim_mf 32 --latent_dim_mlp 128 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor32neg4-implict_Epoch14_HR0.5227_NDCG0.2938.model'
# echo "NEUMF NO PRETRAIN FACTOR 64"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_no_pretrain_factor64neg4' --layers '512,256,128,64' --num_epoch 100 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --latent_dim_mf 64 --latent_dim_mlp 256 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor64neg4-implict_Epoch14_HR0.5252_NDCG0.2943.model'

#-------PININTEREST WITH PRETRANING-------------------
# echo "NEUMF PININTEREST FACTOR 8"
# python train.py --dataset 'pininterest' --num_user 55187 --num_items 9916 --model 'NEUMF' --alias 'NEUMF_pininterest_factor8neg4B256L0' --layers '64,32,16,8' --num_epoch 100 --batch_size 256 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 32 --num_negative 4 --l2_regularization 0.0 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/GMF/gmf_pininterest_factor8neg4_Epoch99_HR0.8626_NDCG0.5403.model' --pretrain_mlp 'checkpoints/MLP/mlp_pininterest_factor8neg4V2_Epoch71_HR0.8242_NDCG0.4927.model'
