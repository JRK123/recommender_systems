import torch
import random
import pandas as pd
from copy import deepcopy
from torch.utils.data import DataLoader, Dataset
import numpy as np

random.seed(0)


class UserItemRatingDataset(Dataset):
    """Wrapper, convert <user, item, rating> Tensor into Pytorch Dataset"""
    def __init__(self, user_tensor, item_tensor, target_tensor):
        """
        args:

            target_tensor: torch.Tensor, the corresponding rating for <user, item> pair
        """
        self.user_tensor = user_tensor
        self.item_tensor = item_tensor
        self.target_tensor = target_tensor

    def __getitem__(self, index):
        return self.user_tensor[index], self.item_tensor[index], self.target_tensor[index]

    def __len__(self):
        return self.user_tensor.size(0)


class SampleGenerator(object):
    """Construct dataset for NCF"""

    def __init__(self, ratings):
        """
        args:
            ratings: pd.DataFrame, which contains 4 columns = ['userId', 'itemId', 'rating', 'timestamp']
        """
        assert 'userId' in ratings.columns
        assert 'itemId' in ratings.columns
        assert 'rating' in ratings.columns

        self.ratings = ratings
        # explicit feedback using _normalize and implicit using _binarize
        # self.preprocess_ratings = self._normalize(ratings)
        self.preprocess_ratings = self._binarize(ratings)
        self.user_pool = set(self.ratings['userId'].unique())
        self.item_pool = set(self.ratings['itemId'].unique())
        # create negative item samples for NCF learning
        print('negative sample in')
        self.negatives = self._sample_negative(ratings)
        print('negative samples out')
        self.train_ratings, self.test_ratings = self._split_loo(self.preprocess_ratings)
        print("train num user", self.train_ratings['userId'].nunique())
        print("trian_item_user", self.train_ratings.itemId.max())
        
        self.trainUserMax = self.train_ratings.userId.max()
        self.trainItemMax = self.train_ratings.itemId.max()
        self.trainMatrix =np.zeros((self.trainUserMax+1, self.trainItemMax+1))
        for row in self.train_ratings.itertuples():
            self.trainMatrix[row.userId][row.itemId] = 1


    def _normalize(self, ratings):
        """normalize into [0, 1] from [0, max_rating], explicit feedback"""
        ratings = deepcopy(ratings)
        max_rating = ratings.rating.max()
        ratings['rating'] = ratings.rating * 1.0 / max_rating
        return ratings
    
    def _binarize(self, ratings):
        """binarize into 0 or 1, imlicit feedback"""
        ratings = deepcopy(ratings)
        ratings['rating'][ratings['rating'] > 0] = 1.0
        return ratings

    def _split_loo(self, ratings):
        """leave one out train/test split """
        ratings['rank_latest'] = ratings.groupby(['userId'])['timestamp'].rank(method='first', ascending=False)
        # print((ratings))
        test = ratings[ratings['rank_latest'] == 1.0]
        train = ratings[ratings['rank_latest'] > 1.0]
        print(set(test['userId'].drop_duplicates()).difference(set(train['userId'].drop_duplicates())))
        print("test Len :", len(test['userId'].drop_duplicates()))
        print("train Len :", len(train['userId'].drop_duplicates()))
        assert train['userId'].nunique() == test['userId'].nunique()
        return train[['userId', 'itemId', 'rating']], test[['userId', 'itemId', 'rating']]

    def _sample_negative(self, ratings):
        """return all negative items & 100 sampled negative items"""
        interact_status = ratings.groupby('userId')['itemId'].apply(set).reset_index().rename(
            columns={'itemId': 'interacted_items'})
        # print('step -1')
        # interact_status['negative_items'] = interact_status['interacted_items'].apply(lambda x: self.item_pool - x)
        negativeSampleSetList = []
        for elem in interact_status['interacted_items']:
            negativeSet = self.item_pool - elem
            negativeSampleSetList.append(random.sample(negativeSet, 99))
        # print("negative list : \n", negativeSetList)
        # interact_status['negative_items'] = negativeSetList
        # interact_status['negative_items'] = negativeSetList
        # print('step -2')
        # interact_status['negative_samples'] = interact_status['negative_items'].apply(lambda x: random.sample(x, 99))
        interact_status['negative_samples'] = negativeSampleSetList
        # return interact_status[['userId', 'negative_items', 'negative_samples']]
        return interact_status

    def instance_a_train_loader(self, num_negatives, batch_size):
        """instance train loader for one training epoch"""# what is the need to have it for one traning epoch
        users, items, ratings = [], [], []
        # train_ratings = pd.merge(self.train_ratings, self.negatives[['userId', 'negative_items']], on='userId')
        # train_ratings = self.negatives[['userId']]
        train_ratings = self.train_ratings
        # train_ratings['negatives'] = train_ratings['negative_items'].apply(lambda x: random.sample(x, num_negatives))
        for row in train_ratings.itertuples():
            # negativeList = self.item_pool - self.negatives['interacted_items'][int(row.userId)]  
            # print(negativeList)
            # negatives = random.sample(negativeList, num_negatives)
            # print(negatives)
            users.append(int(row.userId))
            items.append(int(row.itemId))
            ratings.append(float(row.rating))
            t = num_negatives
            # for i in range(num_negatives):
            while(t):
                r = random.randint(0, self.trainItemMax)
                # print(r)
                if self.trainMatrix[row.userId][r] == 1:
                    continue
                # print("generated random", t)
                # num_negatives -= 1
                t = t -1
                users.append(int(row.userId))
                # items.append(negatives[i])
                items.append(r)
                ratings.append(float(0))  # negative samples get 0 rating
        dataset = UserItemRatingDataset(user_tensor=torch.LongTensor(users),
                                        item_tensor=torch.LongTensor(items),
                                        target_tensor=torch.FloatTensor(ratings))
        # print("created dataset")
        return DataLoader(dataset, batch_size=batch_size, shuffle=True)

    @property
    def evaluate_data(self):
        """create evaluate data"""
        test_ratings = pd.merge(self.test_ratings, self.negatives[['userId', 'negative_samples']], on='userId')
        test_users, test_items, negative_users, negative_items = [], [], [], []
        for row in test_ratings.itertuples():
            test_users.append(int(row.userId))
            test_items.append(int(row.itemId))
            for i in range(len(row.negative_samples)):
                negative_users.append(int(row.userId))
                negative_items.append(int(row.negative_samples[i]))
        print("TEST USER LENGTH: ",len(test_users))
        return [torch.LongTensor(test_users), torch.LongTensor(test_items), torch.LongTensor(negative_users),
                torch.LongTensor(negative_items)]
