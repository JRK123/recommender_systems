#!/bin/sh
#SBATCH --job-name 64nuep256 # Job name
#SBATCH --ntasks=1 # Run on a single CPU
#SBATCH --time=48:00:00 # Time limit hrs:min:sec
#SBATCH --output=test_job%j.out # Standard output and error log
#SBATCH --gres=gpu:1
#SBATCH --partition=low_unl_1gpu 
./scripts/256/nuemf_pin_64_256_0.sh
