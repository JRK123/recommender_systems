#!/bin/sh
#SBATCH --job-name=test_job # Job name
#SBATCH --ntasks=1 # Run on a single CPU
#SBATCH --time=10:00:00 # Time limit hrs:min:sec
#SBATCH --output=./output/gmf%j.out # Standard output and error log
#SBATCH --gres=gpu:1
#SBATCH --partition=low_2h_1gpu
#This cell generates the results for GMF factor 8
echo "GMF FACTOR 8"
python train.py --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_factor8neg4-implict' --num_epoch 20 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
echo "GMF FACTOR 16"
python train.py --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_factor16neg4-implict' --num_epoch 20 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model' 
echo "GMF FACTOR 32"
python train.py --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_factor32neg4-implict' --num_epoch 20 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
echo "GMF FACTOR 64"
python train.py --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_factor64neg4-implict' --num_epoch 20 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'


echo "MLP FACTOR 8"
python train.py --num_user 1208 --num_items 3539 --model 'MLP' --alias 'cu_mlp_factor8neg4-implict'  --layers '16,64,32,16,8' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
echo "MLP FACTOR 16"
python train.py --num_user 1208 --num_items 3539 --model 'MLP' --alias 'cu_mlp_factor16neg4-implict' --layers '16,128,64,32,16' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model' 
echo "MLP FACTOR 32"
python train.py --num_user 1208 --num_items 3539 --model 'MLP' --alias 'cu_mlp_factor32neg4-implict' --layers '16,256,128,64,32' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
echo "MLP FACTOR 64"
python train.py --num_user 1208 --num_items 3539 --model 'MLP' --alias 'cu_mlp_factor64neg4-implict' --layers '16, 512,256,128,64' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 #--pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

#nuemf with pretrain: 
# echo "NEUMF FACTOR 8"
# python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_factor8neg4-implict'  --layers '16,64,32,16,8' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model'
# echo "NEUMF FACTOR 16"
# python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_factor16neg4-implict' --layers '16,128,64,32,16' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor16neg4-implict_Epoch14_HR0.5197_NDCG0.2938.model'
# echo "NEUMF FACTOR 32"
# python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_factor32neg4-implict' --layers '16,256,128,64,32' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor32neg4-implict_Epoch14_HR0.5227_NDCG0.2938.model'
# echo "NEUMF FACTOR 64"
# python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_factor64neg4-implict' --layers '16, 512,256,128,64' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 1 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor64neg4-implict_Epoch14_HR0.5252_NDCG0.2943.model'

#nuemf without pretraning : 
echo "NEUMF FACTOR 8"
python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_no_pretrain_factor8neg4-implict'  --layers '16,64,32,16,8' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model'
echo "NEUMF FACTOR 16"
python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_no_pretrain_factor16neg4-implict' --layers '16,128,64,32,16' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor16neg4-implict_Epoch14_HR0.5197_NDCG0.2938.model'
echo "NEUMF FACTOR 32"
python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_no_pretrain_factor32neg4-implict' --layers '16,256,128,64,32' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor32neg4-implict_Epoch14_HR0.5227_NDCG0.2938.model'
echo "NEUMF FACTOR 64"
python train.py --num_user 1208 --num_items 3539 --model 'NEUMF' --alias 'cu_NEUMF_no_pretrain_factor64neg4-implict' --layers '16, 512,256,128,64' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --latent_dim_mf 8 --latent_dim_mlp 8 --num_negative 4 --l2_regularization 0.0000001 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model' --pretrain_mlp 'checkpoints/mlp_factor64neg4-implict_Epoch14_HR0.5252_NDCG0.2943.model'


##------PININTEREST-------------
# echo "GMF FACTOR 8"
# python train.py --num_user 55187 --num_items 9863 --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_pininterest_factor8neg4-implict' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 8 --num_negative 4 --l2_regularization 0 --device_id 2 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# python train.py --num_user 55187 --num_items 9863 --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_pininterest_factor16neg4-implict' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 16 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# python train.py --num_user 55187 --num_items 9863 --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_pininterest_factor32neg4-implict' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 32 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'

# python train.py --num_user 55187 --num_items 9863 --num_user 1208 --num_items 3539 --model 'GMF' --alias 'cu_gmf_pininterest_factor64neg4-implict' --num_epoch 15 --batch_size 1024 --optimizer 'adam' --latent_dim 64 --num_negative 4 --l2_regularization 0 --device_id 0 --pretrain 0 --pretrain_mf 'checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'
