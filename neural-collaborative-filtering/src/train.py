from ast import alias
from fileinput import filename
import pandas as pd
import numpy as np
from gmf import GMFEngine
from mlp import MLPEngine
from neumf import NeuMFEngine
from data import SampleGenerator
import argparse
import csv
def parse_args():
    parser = argparse.ArgumentParser(description="Run NeuMF.")
    parser.add_argument('--alias', nargs='?', default='gmf_factor8neg4-implict',
                        help='Input name to save model')
    parser.add_argument('--num_epoch', type=int, default='15',
                        help='set epoch')
    parser.add_argument('--batch_size', type=int, default=256,
                        help='Batch size.')
    parser.add_argument('--optimizer', nargs='?', default='adam',
                        help='optimizer')
    parser.add_argument('--adam_lr', type=float, default=1e-3,
                        help="adam learning rate")
    parser.add_argument('--num_users', type=int, default=6040,
                        help='Number of users')                    
    parser.add_argument('--num_items', type=int, default=3706,
                        help="Regularization for each MLP layer. reg_layers[0] is the regularization for embeddings.")
    parser.add_argument('--latent_dim', type=int, default=8,
                        help='Number of negative instances to pair with a positive instance.')
    parser.add_argument('--latent_dim_mf', type=int, default=8,
                        help='Number of negative instances to pair with a positive instance.')
    parser.add_argument('--latent_dim_mlp', type=int, default=8,
                    help='Number of negative instances to pair with a positive instance.')
    parser.add_argument('--num_negative', type=int, default=4,
                        help='Learning rate.')
    parser.add_argument('--l2_regularization', type=float, default=0,
                        help='Specify an optimizer: adagrad, adam, rmsprop, sgd')
    parser.add_argument('--use_cuda', type=bool, default=True,
                        help='Show performance per X iterations')
    parser.add_argument('--device_id', type=int, default=0,
                        help='Whether to save the trained model.')
    parser.add_argument('--mlp_pretrain', nargs='?', default='',
                        help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--pretrain', type=int, default=1,
                        help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--layers', nargs='?', default='16,64,32,16,8',
                        help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--model', nargs='?', default='GMF',
                        help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--pretrain_mf', nargs='?', default='checkpoints/gmf_factor8neg4-implict_Epoch19_HR0.6106_NDCG0.3501.model',
                    help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--pretrain_mlp', nargs='?', default='checkpoints/mlp_factor8neg4-implict_Epoch14_HR0.5007_NDCG0.2762.model',
                    help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--dataset', nargs='?', default='movielens',
                        help='Specify the pretrain model file for MLP part. If empty, no pretrain will be used')
    parser.add_argument('--sgd_lr', type=float, default=1e-3,
                        help="adam learning rate")
    parser.add_argument('--sgd_momentum', type=float, default=0,
                        help="adam learning rate")
    return parser.parse_args()


args = parse_args()
# print(args.pretrain)
if args.pretrain == 0:
    pretrain = False
else :
    pretrain = True
testlist = args.layers.split(',')
testlist = list(map(int, testlist))
config = {
    'alias': args.alias,
    'num_epoch': args.num_epoch,
    'batch_size': args.batch_size,
    # 'optimizer': 'sgd',
    'sgd_lr': args.sgd_lr,
    'sgd_momentum': args.sgd_momentum,
    # 'optimizer': 'rmsprop',
    # 'rmsprop_lr': 1e-3,
    # 'rmsprop_alpha': 0.99,
    # 'rmsprop_momentum': 0,
    'optimizer': args.optimizer,
    'adam_lr': args.adam_lr,
    'num_users': args.num_users,
    'num_items': args.num_items,
    'latent_dim': args.latent_dim,
    'num_negative': args.num_negative,
    'l2_regularization': args.l2_regularization, # 0.01
    'use_cuda': args.use_cuda,
    'device_id': args.device_id,
    'layers' : testlist,
    'model_dir':'checkpoints/'+str(args.model) + '/' + '{}_Epoch{}_HR{:.4f}_NDCG{:.4f}.model',
    'pretrain' : pretrain,
    'pretrain_mf': args.pretrain_mf,
    'latent_dim_mf': args.latent_dim_mf,
    'latent_dim_mlp' : args.latent_dim_mlp,
    'pretrain_mf' : args.pretrain_mf,
    'pretrain_mlp': args.pretrain_mlp
}

# if type(args.layers) != list: args.layers = list(map(int, args.layers.split(','))) 

# gmf_config = {'alias': 'gmf_factor8neg4-implict',
#               'num_epoch': 20,
#               'batch_size': 1024,
#               # 'optimizer': 'sgd',
#               # 'sgd_lr': 1e-3,
#               # 'sgd_momentum': 0.9,
#               # 'optimizer': 'rmsprop',
#               # 'rmsprop_lr': 1e-3,
#               # 'rmsprop_alpha': 0.99,
#               # 'rmsprop_momentum': 0,
#               'optimizer': 'adam',
#               'adam_lr': 1e-3,
#               'num_users': 6040,
#               'num_items': 3706,
#               'latent_dim': 8,
#               'num_negative': 4,
#               'l2_regularization': 0, # 0.01
#               'use_cuda': True,
#               'device_id': 0,
#               'model_dir':'checkpoints/{}_Epoch{}_HR{:.4f}_NDCG{:.4f}.model'}

# mlp_config = {'alias': 'mlp_factor8neg4_bz256_166432168_pretrain_reg_0.0000001',
#               'num_epoch': 15,
#               'batch_size':256,
#               'optimizer': 'adam',
#               'adam_lr': 1e-3,
#               'num_users': 6040,
#               'num_items': 3706,
#               'latent_dim': 8,
#               'num_negative': 4,
#               'layers': [16,64,32,16,8],#[16,256,128,64,32],#[16,256,128,64,32],#[16,128,64,32,16],#[16,64,32,16,8],  # layers[0] is the concat of latent user vector & latent item vector
#               'l2_regularization': 0.0000001,  # MLP model is sensitive to hyper params
#               'use_cuda': True,
#               'device_id': 0,
#               'pretrain': False,
#               'pretrain_mf': 'checkpoints/{}'.format('gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'),
#               'model_dir':'checkpoints/{}_Epoch{}_HR{:.4f}_NDCG{:.4f}.model'}

# neumf_config = {'alias': 'pretrain_neumf_factor8neg4',
#                 'num_epoch': 15,
#                 'batch_size': 1024,
#                 'optimizer': 'adam',
#                 'adam_lr': 1e-3,
#                 'num_users': 6040,
#                 'num_items': 3706,
#                 'latent_dim_mf': 8,
#                 'latent_dim_mlp': 8,
#                 'num_negative': 4,
#                 'layers': [16,64,32,16,8],  # layers[0] is the concat of latent user vector & latent item vector
#                 'l2_regularization': 0.000001,
#                 'use_cuda': True,
#                 'device_id': 0,
#                 'pretrain': True,
#                 'pretrain_mf': 'checkpoints/{}'.format('gmf_factor8neg4-implict_Epoch19_HR0.6141_NDCG0.3438.model'),
#                 'pretrain_mlp': 'checkpoints/{}'.format('mlp_factor8neg4_bz256_166432168_pretrain_reg_0.0000001_Epoch14_HR0.5561_NDCG0.3142.model'),
#                 'model_dir':'checkpoints/{}_Epoch{}_HR{:.4f}_NDCG{:.4f}.model'
#                 }

# Load Data
if args.dataset == 'movielens':
    print('READING MOVIELENS DATASET')
    ml1m_dir = 'data/ml-1m/ratings.dat'
    ml1m_rating = pd.read_csv(ml1m_dir, sep='::', header=None, names=['uid', 'mid', 'rating', 'timestamp'],  engine='python')
else :
    print("READING PININTEREST DATASET")
    ml1m_dir = 'data/pin-interest-main.txt'
    ml1m_rating = pd.read_csv(ml1m_dir, delim_whitespace=True, header=None, names=['uid', 'mid', 'rating', 'timestamp'],  engine='python')

# Reindex
user_id = ml1m_rating[['uid']].drop_duplicates().reindex()
user_id['userId'] = np.arange(len(user_id))
ml1m_rating = pd.merge(ml1m_rating, user_id, on=['uid'], how='left')
item_id = ml1m_rating[['mid']].drop_duplicates().reindex()
item_id['itemId'] = np.arange(len(item_id))
ml1m_rating = pd.merge(ml1m_rating, item_id, on=['mid'], how='left')
ml1m_rating = ml1m_rating[['userId', 'itemId', 'rating', 'timestamp']]
print('Range of userId is [{}, {}]'.format(ml1m_rating.userId.min(), ml1m_rating.userId.max()))
print('Range of itemId is [{}, {}]'.format(ml1m_rating.itemId.min(), ml1m_rating.itemId.max()))
# # DataLoader for training
sample_generator = SampleGenerator(ratings=ml1m_rating)
print("-- sample generator--")
evaluate_data = sample_generator.evaluate_data
# Specify the exact model
# config = gmf_config
if (args.model == 'GMF') :
    engine = GMFEngine(config)
# config = mlp_config
if (args.model == 'MLP') :
    engine = MLPEngine(config)
# config = neumf_config
if (args.model == 'NEUMF') :
    engine = NeuMFEngine(config)
hit_ratio = 0
ndcg = 0

filename = './results/' + str(config['batch_size']) + '/' + str(args.model) + '/' +str(config['alias']) +'.csv'
outfile = open(filename,'w+', newline='')
header = ['epoch', 'loss', 'hitRatio', 'ndcg']
out = csv.writer(outfile)
out.writerow(header)
for epoch in range(config['num_epoch']):
    print('Epoch {} starts !'.format(epoch))
    print('-' * 80)
    train_loader = sample_generator.instance_a_train_loader(config['num_negative'], config['batch_size'])
    # print(train_loader)
    loss = engine.train_an_epoch(train_loader, epoch_id=epoch)
    hit_ratio, ndcg = engine.evaluate(evaluate_data, epoch_id=epoch)
    data = [[epoch, loss, hit_ratio, ndcg]]
    out.writerows(map(lambda x: x, data))
    print("HIT RATIO : {}, NDCG : {}".format(hit_ratio, ndcg))
    engine.save(config['alias'], epoch, hit_ratio, ndcg)
outfile.close()